<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    /** @test */
    public function user_can_show_task_if_task_exist()
    {
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.show', $task->id));
        $response->assertViewIs('tasks.show');
        $response->assertSee($task->name);
    }

    /** @test */
    public function user_cannot_show_task_if_task_not_exist()
    {
        $taskId = -1;
        $response = $this->get(route('tasks.show', $taskId));
        $response->assertStatus(404);
    }
}
