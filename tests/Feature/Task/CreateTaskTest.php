<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    use WithFaker;
    /** @test */
    public function user_can_create_task_if_data_validate()
    {
        $dataCreate = ['name' => $this->faker->name(), 'content' => $this->faker->text() ];
        $countTaskBefore = Task::count();
        $response = $this->post(route('tasks.store'), $dataCreate);
        $response->assertRedirect(route('tasks.index'));
        $countTaskAfter = Task::count();
        $this->assertDatabaseHas('tasks', ['name' => $dataCreate['name'], 'content' => $dataCreate['content']]);
        $this->assertEquals($countTaskAfter-1, $countTaskBefore);
    }

    /** @test */
    public function user_cannot_create_task_if_data_not_validate()
    {
        $dataCreate = ['name' => null, 'content' => $this->faker->text() ];
        $response = $this->post(route('tasks.store'), $dataCreate);
        $response->assertSessionHasErrors('name');
    }
}
